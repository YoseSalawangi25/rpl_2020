<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Model{
    public function getToko(){
        $arr_toko[] = array(1,'Triplek',30,150000);
        $arr_toko[] = array(2,'Pasir',1000,3000);
        $arr_toko[] = array(3,'Semen',500,150000);
        $arr_toko[] = array(4,'Ubin',500,79000);
        $arr_toko[] = array(5,'Paku',200,2500);
        $arr_toko[] = array(6,'Gerobak',2,350000);
        $arr_toko[] = array(1,'Semen Putih',400,150000);
        $arr_toko[] = array(2,'Pipa',10,3000);
        $arr_toko[] = array(3,'Seng',50,150000);
        $arr_toko[] = array(4,'Tandon Air',50,79000);
        $arr_toko[] = array(5,'Keran Air',20,2500);
        $arr_toko[] = array(6,'Kayu',700,350000);
        return $arr_toko;
    }
}