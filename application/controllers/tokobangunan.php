<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TokoBangunan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() //langsung memanggil nama controler
	{
        $this->load->view('tokobangunan_View');
    }
    
    public function tugas1(){ 
        $this->load->Model('Toko_Bangunan_Abimanyu');

        $data['toko_bangunan_abimanyu'] = $this->Toko->getToko();
        $this->load->view('TokoBangunan_View', $data);
    }
}